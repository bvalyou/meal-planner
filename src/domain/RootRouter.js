import { useRoutes } from 'hookrouter';
import React from 'react';
import GoShopping from './GoShopping';
import HomePage from './HomePage';
import NotFound from './NotFound';

const makeRoute = Component => params => <Component {...params} />;

const routes = {
	'/': makeRoute(HomePage),
	'/go-shopping': makeRoute(GoShopping),
};

const RootRouter = () => useRoutes(routes) || <NotFound />;

export default RootRouter;
